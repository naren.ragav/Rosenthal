C++ code to calculate temperature profile of rosenthal analytical equation

Modify rosenthal.cc to change the input parameters of the welding conditions, material property,etc.<br />
Modify plot.py to change the ploting parameters.

Output data<br />
output_2D.csv contains the 2D temperature data of the top surface<br />
output_3D.csv contains the 3D temperature data of the 3D domain<br />
output.png is the heat map of 2D surface temperature<br />

To delete previous output data: make clean<br />
To create new output data: make all<br />

