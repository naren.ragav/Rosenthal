CC=g++

all: rosenthal.out filegen plot.py
	python plot.py

rosenthal.out: rosenthal.cc
	$(CC) rosenthal.cc -o rosenthal.out

filegen: rosenthal.out
	./rosenthal.out

clean:
	rm -f *.png *.out *.csv
